import os
from dotenv import load_dotenv
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from app import create_app
from app.core.data.models.consultancies_and_advisory.consult import db as consult_db
from app.core.data.models.standards_and_regulations.association import db as association_db 
from app.core.data.models.standards_and_regulations.standard import db as standard_db 
from app.core.data.models.standards_and_regulations.requirement import db as requirement_db 

load_dotenv(verbose=True)

if os.getenv('APP_SETTINGS') is None:
    from pathlib import Path
    load_dotenv(dotenv_path='/home/ubuntu/compliance/.prod_env')

app = create_app(config_name=os.getenv('APP_SETTINGS'))
migrate = Migrate(app, consult_db)
migrate = Migrate(app, requirement_db)
migrate = Migrate(app, standard_db)
migrate = Migrate(app, association_db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()