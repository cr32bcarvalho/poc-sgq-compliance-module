activate_this = '/home/ubuntu/compliance/.venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys
sys.path.insert(0, '/home/ubuntu/compliance')

from application import application
