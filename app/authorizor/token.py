from functools import wraps
import urllib.request
import json
from flask import request, jsonify, abort
from app.models.user import User, UserType, db

post_url = 'https://tfuj712nmg.execute-api.us-west-2.amazonaws.com/auth/decode-token'

def token_required(role=None):
   def decorator(f):
      @wraps(f)
      def decorated_function(*args, **kwargs):
         token = None

         if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']

         if not token:
            return jsonify({'message': 'a valid token is missing'})
         
         req = urllib.request.Request(post_url)
         req.add_header('Content-Type', 'application/json; charset=utf-8')
         
         post_data = json.dumps({"token": token})
         post_data_as_bytes = post_data.encode('utf-8')   

         req.add_header('Content-Length', len(post_data_as_bytes))
         post_response = urllib.request.urlopen(req, post_data_as_bytes)

         res_body = post_response.read()
         data = json.loads(res_body.decode("utf-8"))
         current_user = User.query.filter_by(public_id=data.get('sub')).first()
   
         if current_user is None:
            user = User(name = data.get('name'))
            user.public_id = data.get('sub')
            user.email = data.get('email')
            user.department = data.get('custom:department')
            db.session.add(user)
            db.session.commit()
            current_user = user
         else: 
            force_update = False
            if(current_user.name != data.get('name')):
               force_update = True
               current_user.name = data.get('name')
           
            if(current_user.email != data.get('email')):
               force_update = True
               current_user.email = data.get('email')

            if(current_user.department != data.get('custom:department')):
               force_update = True
               current_user.department = data.get('custom:department')
            
            if force_update:
               db.session.commit()

         if autorized(current_user.department, role) is False:
            return abort(401)

         if current_user is None:
            return jsonify({'message': 'token is invalid'})

         return f(current_user, *args, **kwargs)
      return decorated_function
   return decorator

def autorized(userType, userRequired):
   if userType == None:
      return False
   
   if userRequired == None or userType == UserType.admin:
      return True

   if userRequired == userType:
      return True

   return False

