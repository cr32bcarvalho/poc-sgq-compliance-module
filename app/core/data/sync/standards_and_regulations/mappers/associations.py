import json
from app.core.data.models.standards_and_regulations.association import db as db_association, Association, Status as StatusAssociation
from sqlalchemy.orm import sessionmaker

Session = sessionmaker()

def mapperObject(object):
    association = Association(object.get('razao_social'))
    association.id = object.get('id')
    association.nif = object.get('cnpj')
    association.nickname = object.get('nome_fantasia')
    association.address = object.get('logradouro')
    # association.address_number = object.get('numero_logradouro')
    association.complement = object.get('complemento_logradouro')
    association.neighborhood = object.get('bairro')
    association.zipcode = object.get('cep')
    if object.get('ativo') is False:
        association.status = StatusAssociation.inactive
    return association

def run(data):
    if data.get('_embedded', None) is None:
        associations = [mapperObject(data)]
    else:
        associations = list(map(mapperObject, data['_embedded']['fornecedores']))

    for association in associations:
        older = Association.query.filter_by(id=str(association.id)).first()
        if older is not None:
            association.uuid = older.uuid
            association.status = older.status

        db_association.session.merge(association)
            
    db_association.session.commit()

    return Association.query.all()
  