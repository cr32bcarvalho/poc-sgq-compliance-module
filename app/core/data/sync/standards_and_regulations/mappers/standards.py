import json
from app.core.data.models.standards_and_regulations.standard import db as db_standard, Standard
from app.core.data.models.standards_and_regulations.association import Association

def mapperObject(object):
    standard = Standard(object.get('title'))
    standard.association_id = object.get('association_id')
    standard.id = object.get('id')
    standard.resume = object.get('resume')
    standard.status = object.get('status')
    standard.committee = object.get('committee')
    standard.valid_from = object.get('valid_from')
    standard.published_at = object.get('published_at')
    return standard

def run(data):
    if data.get('_embedded', None) is None:
        standards = [mapperObject(data)]
    else:
        standards = list(map(mapperObject, data['_embedded']['standards']))

    for standard in standards:
        association = Association.query.filter_by(id=str(standard.association_id)).first()
        standard.association_uuid = association.uuid
        older = Standard.query.filter_by(id=str(standard.id)).first()

        if older is not None:
            standard.uuid = older.uuid

        db_standard.session.merge(standard)
            
    db_standard.session.commit()

    return Standard.query.all()
  