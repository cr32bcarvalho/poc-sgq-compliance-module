import json
from app.core.data.models.standards_and_regulations.requirement import db as db_requirement, Requirement
from app.core.data.models.standards_and_regulations.standard import Standard

def mapperObject(object):
    requirement = Requirement(object.get('title'))
    requirement.id = object.get('id')
    requirement.standard_id = object.get('standard_id')
    requirement.description = object.get('description')
    requirement.valid_from = object.get('valid_from')
    requirement.published_at = object.get('published_at')
    requirement.status = object.get('status')

    return requirement

def run(data):
    if data.get('_embedded', None) is None:
        requirements = [mapperObject(data)]
    else:
        requirements = list(map(mapperObject, data['_embedded']['requirements']))

    for requirement in requirements:
        standard = Standard.query.filter_by(id=str(requirement.standard_id)).first()
        requirement.standard_uuid = standard.uuid
        older = Requirement.query.filter_by(id=str(requirement.id)).first()

        if older is not None:
            requirement.uuid = older.uuid

        db_requirement.session.merge(requirement)
            
    db_requirement.session.commit()

    return Requirement.query.all()
  