from flask import Blueprint
from flask import current_app as app
from flask import abort, jsonify, request, Response
from app.core.data.models.standards_and_regulations.standard import db as db_standard, Standard
from app.core.data.models.standards_and_regulations.standard.schema import StandardSchema
from app.core.data.models.standards_and_regulations.requirement import db as db_requirement, Requirement
from app.core.data.models.standards_and_regulations.requirement.schema import RequirementSchema
from app.core.data.models.standards_and_regulations.association.schema import AssociationSchema
from app.core.data.sync.sync_request import fetch_data
from .mappers.associations import run as update_associations
from .mappers.associations import run as update_associations
from .mappers.standards import run as update_standards
from .mappers.requirements import run as update_requirements

standards_and_regulations_sync_bp = Blueprint('standards_and_regulations_sync_bp', __name__, url_prefix='/sync/standards-and-regulations')

@standards_and_regulations_sync_bp.route('/associations', methods=['GET'])
def get_associations():
    standard_url = "https://sgq-build.s3.us-west-2.amazonaws.com/static/sample_association.json"
    associations = fetch_data(standard_url, update_associations)
    schema = AssociationSchema(many=True)
    result = schema.dump(associations)
    return {"associations": result}, 200

@standards_and_regulations_sync_bp.route('/standards', methods=['GET'])
def get_standards():
    standard_url = "https://sgq-build.s3-us-west-2.amazonaws.com/static/sample_standard.json"
    standards = fetch_data(standard_url, update_standards)
    schema = StandardSchema(many=True)
    result = schema.dump(standards)
    return {"standards": result}, 200

@standards_and_regulations_sync_bp.route('/requirements', methods=['GET'])
def get_requirements():
    requirement_url = "https://sgq-build.s3-us-west-2.amazonaws.com/static/sample_requirement.json"
    requirements = fetch_data(requirement_url, update_requirements)
    schema = RequirementSchema(many=True)
    result = schema.dump(requirements)
    return {"requirements": result}, 200
