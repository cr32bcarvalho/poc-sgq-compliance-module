from flask import Blueprint
from flask import current_app as app
from flask import abort, jsonify, request, Response
from app.core.data.models.consultancies_and_advisory.consult import db, Consult, Status
from app.core.data.models.consultancies_and_advisory.consult.schema import ConsultSchema
from app.core.data.sync.sync_request import fetch_data
from .mappers.consult import run as map_and_update

consultancies_and_advisory_sync_bp = Blueprint('consultancies_and_advisory_sync_bp', __name__, url_prefix='/sync/consultancies-and-advisory')

@consultancies_and_advisory_sync_bp.route('/', defaults={'uuid': None}, methods=['GET'])
@consultancies_and_advisory_sync_bp.route('/<uuid>', methods=['GET'])
def get(uuid):
    consultancies_url = "https://sgq-build.s3.us-west-2.amazonaws.com/static/sample_consultancies_and_advisory.json"
    consultancies = fetch_data(consultancies_url, map_and_update)
    schema = ConsultSchema(many=True)
    result = schema.dump(consultancies)
    return {"consultancies": result}, 200