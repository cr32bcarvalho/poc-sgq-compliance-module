import json
from app.core.data.models.consultancies_and_advisory.consult import db as db_consult, Consult, Status as StatusConsult
from sqlalchemy.orm import sessionmaker

Session = sessionmaker()

def mapperObject(object):
    consult = Consult(object.get('nome'))
    consult.id = object.get('id')
    
    if object.get('cnpj') is None:
        consult.nif = object.get('cpf')
    else:
        consult.nif = object.get('cnpj')

    if object.get('ativo') is False:
        consult.status = StatusConsult.inactive
    return consult

def run(data):
    if data.get('_embedded', None) is None:
        consultancies = [mapperObject(data)]
    else:
        consultancies = list(map(mapperObject, data['_embedded']['fornecedores']))

    for consult in consultancies:
        older = Consult.query.filter_by(id=str(consult.id)).first()
        if older is not None:
            consult.uuid = older.uuid
            consult.status = older.status
            db_consult.session.merge(consult)
        else:
            db_consult.session.merge(consult)

    db_consult.session.commit()
    
    return Consult.query.all()
  