import urllib.request
import json

def fetch_data(url, mapper, body = None):
  req = urllib.request.Request(url)
  req.add_header('Content-Type', 'application/json; charset=utf-8')
         
  if body is not None:
    post_data = json.dumps(body)
    post_data_as_bytes = post_data.encode('utf-8')   
    req.add_header('Content-Length', len(post_data_as_bytes))
    post_response = urllib.request.urlopen(req, post_data_as_bytes)
  else:
    post_response = urllib.request.urlopen(req)
  
  res_body = post_response.read()
  data = json.loads(res_body.decode("utf-8"))

  return mapper(data)