import uuid 
import json
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import ForeignKey, Enum
from flask_marshmallow import Marshmallow

db = SQLAlchemy() 
ma = Marshmallow()

class Status(enum.Enum):
    active = 'active'
    inactive = 'inactive'
    deleted = 'deleted'

class Association(db.Model):
    __tablename__ = 'associations'

    uuid = db.Column(UUID(as_uuid=True), default=lambda: uuid.uuid4().hex, unique=True, primary_key=True, nullable=False)
    id = db.Column(db.String(50), nullable=False)
    nif = db.Column(db.String(50), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    nickname = db.Column(db.String(100), nullable=True)
    address = db.Column(db.String(100), nullable=True)
    address_number = db.Column(db.String(10), nullable=True)
    neighborhood = db.Column(db.String(50), nullable=True)
    complement = db.Column(db.String(50), nullable=True)
    city = db.Column(db.String(50), nullable=True)
    state = db.Column(db.String(50), nullable=True)
    country = db.Column(db.String(20), nullable=True)
    zipcode = db.Column(db.String(20), nullable=True)
    status = db.Column(Enum(Status), default=Status.active)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp())

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Association: {}>".format(self.name)