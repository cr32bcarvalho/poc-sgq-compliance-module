from app.core.data.models.standards_and_regulations.association import ma, Association, Status
from marshmallow_enum import EnumField

class AssociationSchema(ma.SQLAlchemySchema):
  class Meta:
        model = Association
  uuid = ma.auto_field()
  id = ma.auto_field()
  nif = ma.auto_field()
  name = ma.auto_field()
  nickname = ma.auto_field()
  address = ma.auto_field()
  neighborhood = ma.auto_field()
  complement = ma.auto_field()
  city = ma.auto_field()
  state = ma.auto_field()
  country = ma.auto_field()
  zipcode = ma.auto_field()
  status = EnumField(Status) 
  date_modified = ma.auto_field()
  date_created = ma.auto_field()
