from app.core.data.models.standards_and_regulations.standard import ma, Standard, StandardStatus
from app.core.data.models.standards_and_regulations.association.schema import AssociationSchema
from marshmallow_enum import EnumField

class StandardSchema(ma.SQLAlchemySchema):
  class Meta:
        model = Standard
        include_fk = True
  uuid = ma.auto_field()
  id = ma.auto_field()
  title = ma.auto_field()
  resume = ma.auto_field()
  association = ma.Nested(AssociationSchema)
  status = EnumField(StandardStatus) 
  valid_from = ma.auto_field()
  published_at = ma.auto_field()
  date_modified = ma.auto_field()
  date_created = ma.auto_field()


