from app.core.data.models.standards_and_regulations.requirement import ma, Requirement, RequirementStatus
from app.core.data.models.standards_and_regulations.standard.schema import StandardSchema
from flask_marshmallow.fields import URLFor, Hyperlinks
from marshmallow_enum import EnumField

class RequirementSchema(ma.SQLAlchemySchema):
  class Meta:
        model = Requirement
        include_fk = True
  uuid = ma.auto_field()
  id = ma.auto_field()
  title = ma.auto_field()
  description = ma.auto_field()
  standard = ma.Nested(StandardSchema)
  status = EnumField(RequirementStatus) 
  valid_from = ma.auto_field()
  published_at = ma.auto_field()
  date_modified = ma.auto_field()
  date_created = ma.auto_field()