import uuid 
import json
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey, Enum
from flask_marshmallow import Marshmallow
from app.core.data.models.standards_and_regulations.standard import Standard

db = SQLAlchemy() 
ma = Marshmallow()


class RequirementStatus(enum.Enum):
    active = 'active'
    inactive = 'inactive'
    canceled = 'canceled'

class Requirement(db.Model):
    __tablename__ = 'requirements'

    uuid = db.Column(UUID(as_uuid=True), default=lambda: uuid.uuid4().hex, unique=True, primary_key=True, nullable=False)
    id = db.Column(db.String(50), nullable=False)
    standard_uuid = db.Column(UUID(as_uuid=True), ForeignKey(Standard.uuid))
    standard = relationship(Standard)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(Enum(RequirementStatus), default=RequirementStatus.active)
    published_at = db.Column(db.DateTime, nullable=False)
    valid_from = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp())

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return "<Requirement: {}>".format(self.title)