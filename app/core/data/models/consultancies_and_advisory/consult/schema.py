from marshmallow_enum import EnumField
from app.core.data.models.consultancies_and_advisory.consult import ma, Consult
from app.core.data.models.consultancies_and_advisory.consult import Status

class ConsultSchema(ma.SQLAlchemySchema):
  class Meta:
        model = Consult
  uuid = ma.auto_field()
  id = ma.auto_field()
  nif = ma.auto_field()
  name = ma.auto_field()
  status = EnumField(Status)
  date_modified = ma.auto_field()
  date_created = ma.auto_field()


