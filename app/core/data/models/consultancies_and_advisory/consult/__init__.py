import uuid 
import json
import enum
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Enum
from flask_marshmallow import Marshmallow

db = SQLAlchemy() 
ma = Marshmallow()

class Status(enum.Enum):
    active = 'active'
    inactive = 'inactive'
    deleted = 'deleted'

class Consult(db.Model):
    __tablename__ = 'consultancies_and_advisory'

    uuid = db.Column(UUID(as_uuid=True), default=lambda: uuid.uuid4().hex, unique=True, primary_key=True, nullable=False)
    id = db.Column(db.String(50), nullable=False)
    nif = db.Column(db.String(50), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    status = db.Column(Enum(Status), default=Status.active)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp())

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Consult: {}>".format(self.name)