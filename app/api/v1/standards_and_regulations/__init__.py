from flask import Blueprint
from flask import current_app as app
from flask import abort, jsonify, request, Response
from flask_cors import cross_origin
from app.core.data.models.standards_and_regulations.association import db as db_association, Association, Status as StatusAssociation
from app.core.data.models.standards_and_regulations.association.schema import AssociationSchema
from app.core.data.models.standards_and_regulations.standard import db as db_standard, Standard
from app.core.data.models.standards_and_regulations.standard.schema import StandardSchema
from app.core.data.models.standards_and_regulations.requirement import db as db_requirement, Requirement
from app.core.data.models.standards_and_regulations.requirement.schema import RequirementSchema

standards_and_regulations_bp = Blueprint('standards_and_regulations_bp', __name__, url_prefix='/api/v1/standards-and-regulations')

@cross_origin()
@standards_and_regulations_bp.route('/associations', defaults={'uuid': None}, methods=['GET'])
@standards_and_regulations_bp.route('/associations/<uuid>', methods=['GET'])
def get_associations(uuid):
    if uuid is None:
        associations = Association.query.all()
        schema = AssociationSchema(many=True)
        result = schema.dump(associations)
        return {"associations": result}, 200
    schema = AssociationSchema()
    association = Association.query.filter_by(uuid=uuid).first_or_404()
    return schema.dump(association), 200

@cross_origin()
@standards_and_regulations_bp.route('/standards/<association_uuid>', methods=['GET'])
def get_standards(association_uuid):
    if association_uuid is None:
        abort(404)
    try:
        standards = Standard.query.filter_by(association_uuid=association_uuid).all()
        schema = StandardSchema(many=True)
        result = schema.dump(standards)
        return {"standards": result}, 200
    except:
        abort(404)

@cross_origin()
@standards_and_regulations_bp.route('/standard/<uuid>', methods=['GET'])
def get_standard(uuid):
    if uuid is None:
        abort(404)
    schema = StandardSchema()
    standard = Standard.query.filter_by(uuid=uuid).first_or_404()
    return schema.dump(standard), 200

@cross_origin()
@standards_and_regulations_bp.route('/requirements/<standard_uuid>', methods=['GET'])
def get_requirements(standard_uuid):
    if standard_uuid is None:
        abort(404)
    try:
        requirements = Requirement.query.filter_by(standard_uuid=standard_uuid).all()
        schema = RequirementSchema(many=True)
        result = schema.dump(requirements)
        return {"requirements": result}, 200
    except:
        abort(404)

@cross_origin()
@standards_and_regulations_bp.route('/requirement/<uuid>', methods=['GET'])
def get_requirement(uuid):
    if uuid is None:
        abort(404)
    schema = RequirementSchema()
    requirement = Requirement.query.filter_by(uuid=uuid).first_or_404()
    return schema.dump(requirement), 200