from flask import Blueprint
from flask import abort, jsonify, request, Response
from flask_cors import cross_origin
from app.core.data.models.consultancies_and_advisory.consult import db, Consult, Status
from app.core.data.models.consultancies_and_advisory.consult.schema import ConsultSchema

consultancies_and_advisory_bp = Blueprint('consultancies_and_advisory_bp', __name__, url_prefix='/api/v1/consultancies-and-advisory')

@cross_origin()
@consultancies_and_advisory_bp.route('/consultancies', defaults={'uuid': None}, methods=['GET'])
@consultancies_and_advisory_bp.route('/consultancies/<uuid>', methods=['GET'])
def get(uuid):
    if uuid is None:
        consulting = Consult.query.all()
        schema = ConsultSchema(many=True)
        result = schema.dump(consulting)
        return {"consulting": result}, 200
    schema = ConsultSchema()
    consult = Consult.query.filter_by(uuid=uuid).first_or_404()
    return schema.dump(consult), 200