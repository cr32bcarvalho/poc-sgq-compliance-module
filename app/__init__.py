from flask import Flask
from flask_cors import CORS, cross_origin
from instance.config import app_config
from app.api.v1.consultancies_and_advisory import consultancies_and_advisory_bp
from .core.data.models.consultancies_and_advisory.consult import db as consult_db

from app.api.v1.standards_and_regulations import standards_and_regulations_bp
from app.core.data.models.standards_and_regulations.association import db as association_db 
from app.core.data.models.standards_and_regulations.standard import db as standard_db 
from app.core.data.models.standards_and_regulations.requirement import db as requirement_db 

from app.core.data.sync.standards_and_regulations import standards_and_regulations_sync_bp
from app.core.data.sync.consultancies_and_advisory import consultancies_and_advisory_sync_bp
def create_app(config_name):
    app = Flask(__name__, instance_relative_config=False)
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.config.from_object(app_config[config_name])
    consult_db.init_app(app)  
    association_db.init_app(app)
    standard_db.init_app(app)
    requirement_db.init_app(app)

    with app.app_context():
        consult_db.create_all()
        association_db.create_all()
        standard_db.create_all()
        requirement_db.create_all()

        #consultancies_and_advisory_bp
        app.register_blueprint(consultancies_and_advisory_bp)
        # standards_and_regulations_bp
        app.register_blueprint(standards_and_regulations_bp)

        app.register_blueprint(standards_and_regulations_sync_bp)
        app.register_blueprint(consultancies_and_advisory_sync_bp)
        
        return app
