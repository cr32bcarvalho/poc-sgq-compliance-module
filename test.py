import unittest
import os
from app import create_app

class ConsultanciesTestCase(unittest.TestCase):
    """This class represents the Consultancies test case"""
    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_name=os.getenv('APP_SETTINGS'))
        self.client = self.app.test_client

    def test_api_can_get_consultancies(self):
        """Test API can get a consultancies (GET request)."""
        res = self.client().get('/api/v1/consultancies-and-advisory/consultancies')
        self.assertEqual(res.status_code, 200)

# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()